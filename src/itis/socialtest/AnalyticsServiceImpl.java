package itis.socialtest;

import itis.socialtest.entities.Post;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class AnalyticsServiceImpl implements AnalyticsService {

    @Override
    public List<Post> findPostsByDate(List<Post> posts, String date) {
        List<Post> answerPost = posts.stream().map(x -> {
            if (!x.getDate().equals(date)) {
                x = null;
            }
            return x;
        }).filter(x -> x != null).collect(Collectors.toList());
        return answerPost;
    }

    @Override
    public String findMostPopularAuthorNickname(List<Post> posts) {

        return null;
    }

    @Override
    public Boolean checkPostsThatContainsSearchString(List<Post> posts, String searchString) {
        return posts.stream().anyMatch(x -> x.getContent().contains(searchString));
    }

    @Override
    public List<Post> findAllPostsByAuthorNickname(List<Post> posts, String nick) {
        List<Post> answerPost = posts.stream().map(x -> {
            if (!x.getAuthor().getNickname().equals(nick)) {
                x = null;
            }
            return x;
        }).filter(x -> x != null).collect(Collectors.toList());
        return answerPost;
    }
}