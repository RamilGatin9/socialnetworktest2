package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {
        new MainClass().run("src/itis/socialtest/resources/PostDatabase.csv",
                "src/itis/socialtest/resources/Authors.csv");
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
        File filePosts = new File(postsSourcePath);
        File fileAuthors = new File(authorsSourcePath);
        FileReader fileReaderPosts, fileReaderAuthors;
        try {
            fileReaderPosts = new FileReader(filePosts);
            fileReaderAuthors = new FileReader(fileAuthors);
        } catch (FileNotFoundException e) {
            System.out.println("Ошибка пути");
            return;
        }

        List<Author> authorList = new ArrayList<>();
        allPosts = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(fileReaderAuthors)) {

            while (bufferedReader.ready()) {
                String[] str = bufferedReader.readLine().split(", ");
                Long id = Long.valueOf(str[0]);
                String nickname = str[1];
                String birthdayDate = str[2];
                Author author = new Author(id, nickname, birthdayDate);
                authorList.add(author);
            }

        } catch (IOException e) {
            System.out.println("Ошибка в файле Authors.csv");
            return;
        }

        try (BufferedReader bufferedReader = new BufferedReader(fileReaderPosts)) {

            while (bufferedReader.ready()) {
                String[] str = bufferedReader.readLine().split(", ");

                String date = str[2];
                String content = str[3];
                Long likesCount = Long.valueOf(str[1]);

                int authorId = Integer.valueOf(str[0]);
                Author author = authorList.get(authorId);

                Post post = new Post(date, content, likesCount, author);
                allPosts.add(post);
            }

        } catch (IOException e) {
            System.out.println("Ошибка в файле PostDatabase.csv");
            return;
        }

// 1
        allPosts.stream()
                .forEach((x) -> System.out.println(x.getAuthor().getNickname() + '\n' + x.getContent()));

// 2
        List<Post> postList = analyticsService.findPostsByDate(allPosts, "17.04.2021T10:00");
        postList.stream().forEach(x -> System.out.println(x.getDate() + " " + x.getContent()));

// 3
        postList = analyticsService.findAllPostsByAuthorNickname(allPosts, "varlamov");
        postList.stream()
                .forEach(x -> System.out.println(x.getAuthor().getNickname() + " " + x.getContent()));

//4
        boolean postsThatContainsSearchString = analyticsService
                .checkPostsThatContainsSearchString(allPosts, "Россия");

        System.out.println(postsThatContainsSearchString);

// 5
        String nicknameAuthor = analyticsService.findMostPopularAuthorNickname(allPosts);
        System.out.println(nicknameAuthor);
    }
}